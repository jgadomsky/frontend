import { SubmissionError } from "redux-form";

export default((values) => {
    return fetch("http://localhost:8080/event", {
        method: "POST",
        body: JSON.stringify(values)
    }).then((result) => {
        if (result.ok) {
            alert(`${values.firstName}, You have been signed up for: ${values.eventDate}`);
        }
        return result;
    }).catch(error => {
        if (error.validationErrors) {
            throw new SubmissionError(error.validationErrors);
        } else {
            throw new SubmissionError({_error: "service is down"});
        }
    });
});