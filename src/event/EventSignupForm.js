import React from 'react';
import {Field, reduxForm} from 'redux-form';
import './EventSignupForm.css';
import renderField from '../utils/renderField';
import {required, email} from '../utils/validators';

const EventForm = (props) => {
  const {handleSubmit, submitting, error} = props
  return (
    <form className="event-signup-form" onSubmit={handleSubmit}>
      <Field
        name="firstName"
        type="text"
        component={renderField}
        label="First name"
        validate={required}/>
      <Field
        name="lastName"
        type="text"
        component={renderField}
        label="Last name"
        validate={required}/>
      <Field
        name="email"
        type="email"
        component={renderField}
        label="Email"
        validate={[required, email]}/>
      <Field
        name="eventDate"
        type="date"
        component={renderField}
        label="Event date"
        validate={required}/> 
      {error && <div className="error">
        <div className="error-message">{error}</div>
      </div>}
      <div className="submit-btn-container">
        <button type="submit" className="submit-button" disabled={submitting}>Submit</button>
      </div>
    </form>
  )
}

export default reduxForm({form: 'eventForm'})(EventForm)