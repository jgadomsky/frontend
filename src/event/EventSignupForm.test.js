import React from 'react';
import ReactDOM from 'react-dom';
import EventSignupForm from './EventSignupForm';
import sinon from 'sinon';
import {mount} from 'enzyme';
import {expect} from 'chai'
import {reducer as formReducer} from 'redux-form';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';

describe('EventSignupForm', () => {
  let store,
    handleSubmit,
    subject;

  beforeEach(() => {
    store = createStore(combineReducers({form: formReducer}))
    handleSubmit = sinon
      .stub()
      .returns(Promise.resolve())
    subject = mount(
      <Provider store={store}>
        <EventSignupForm onSubmit={handleSubmit}/>
      </Provider>
    )
  });

  it("validate required fields", () => {
    const form = subject.find('form');
    const requiredFields = ['firstName', 'lastName', 'email', 'eventDate'];
    form.simulate('submit');
    expect(handleSubmit.callCount).to.equal(0);    
    requiredFields.forEach(function (requiredField) {
      let fieldDiv = subject.find(`.form-field.${requiredField}`);
      let validationError = fieldDiv.find('.validation-error');
      expect(validationError.text()).to.be.equal('Field is required');
    });
  });

  it("validate email", () => {
    const form = subject.find('form');
    const emailDiv = subject.find('.form-field.email');
    emailDiv.find('input').simulate('change', {
      target: {
        value: 'test'
      }
    });
    form.simulate('submit');
    const validationError = emailDiv.find('.validation-error');
    expect(validationError.text()).to.be.equal('Invalid email address');
    expect(handleSubmit.callCount).to.equal(0);    
  });

  it("pass validation", () => {
    const form = subject.find('form');
    const firstNameDiv = subject.find('.form-field.firstName');
    const lastNameDiv = subject.find('.form-field.lastName');
    const emailDiv = subject.find('.form-field.email');
    const eventDateDiv = subject.find('.form-field.eventDate');
    firstNameDiv.find('input').simulate('change', {
      target: {
        value: 'Jacek'
      }
    });
    lastNameDiv.find('input').simulate('change', {
      target: {
        value: 'Gadomski'
      }
    });
    emailDiv.find('input').simulate('change', {
      target: {
        value: 'jacek.gadomski@test.pl'
      }
    });
    eventDateDiv.find('input').simulate('change', {
      target: {
        value: '2017-07-10'
      }
    });
    form.simulate('submit');
    expect(handleSubmit.callCount).to.equal(1);
  });
});