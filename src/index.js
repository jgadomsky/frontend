import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import store from './store';
import EventSignupForm from './event/EventSignupForm';
import signOnEvent from './event/signOnEvent';

ReactDOM.render(
  <Provider store={store}>
    <EventSignupForm onSubmit={signOnEvent}/>
  </Provider>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept();
}