import React from 'react';

export default({
  input,
  label,
  type,
  meta: {
    touched,
    error,
    warning
  }
}) => {
  let classes = `form-field ${input.name}`;
  return <div className={classes}>
    <input {...input} placeholder={label} type={type}/> {touched && ((error && <span className="validation-error">{error}</span>) || (warning && <span>{warning}</span>))}
  </div>
}